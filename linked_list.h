
#define LINKEDLIST_HEADER

typedef struct node {
	int data;
	struct node* next;
} Node;

typedef struct list {
	Node* head;
} List;

Node* createNode(int data);
List* createList();
void addNode(int data, List* l);
void deleteNode(int data, List* l);
void destroyList(List* l);
