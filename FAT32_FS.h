#pragma once
#include <stdlib.h>
#include "disk_driver.h"
#include "linked_list.h"



typedef struct {
	int directory_block;
	int block_in_disk;
	char name[128];
	int written_bytes;
	int is_dir;
} FileControlBlock;

typedef struct {	
	FileControlBlock fcb;
	char data[BLOCK_SIZE - sizeof(FileControlBlock)];
} FirstFileBlock;

typedef struct {	
	char data[BLOCK_SIZE];
} FileBlock;

typedef struct {
	FileControlBlock fcb;
	int num_entries;
	int file_blocks[(BLOCK_SIZE - sizeof(FileControlBlock) - sizeof(int)) / sizeof(int)];
} FirstDirectoryBlock;

typedef struct {
	int file_blocks[(BLOCK_SIZE) / sizeof(int)];
} DirectoryBlock;

typedef struct {
	DiskDriver* disk;
	FirstDirectoryBlock* fdp;
} FAT32;

typedef struct {
	FAT32* fat;
	List* list;
	FirstFileBlock* fcb; //puntatore al primo blocco della directory            
	FirstDirectoryBlock* directory;  //puntatore alla directory superiore
	int pos_in_file;                 
} FileHandle; 	
	
typedef struct {
	FAT32* fat;
	FirstDirectoryBlock* dcb;
	FirstDirectoryBlock* directory;
	int pos_in_block;
} DirectoryHandle;

//inizializza un file system su un disco 
//ritorna indietro l'handle della directory posizionata nel livello piu alto 
DirectoryHandle* FAT32_init(FAT32* fat, DiskDriver* disk);

//formattazione del disco
void FAT32_format(FAT32* fat);


//creo un nuovo file vuoto nella directory  
FileHandle* FAT32_createFile(DirectoryHandle* dir, const char* filename);

//leggo tutti i nomi dei file presenti nella directory 
int FAT32_readDir(char** names, DirectoryHandle* dir);

//apro un file nella directory 
FileHandle* FAT32_openFile(DirectoryHandle* dir, const char* filename);

//chiude un file handle e lo distrugge 
int FAT32_closeFile(FileHandle* f);

//scrive su file size bytes dalla posizione corrente, in caso si alloca nuovo spazio se necessario
//ritorna il numero di bytes scritti
int FAT32_write(FileHandle* f, void* data, int size);

//ritorna il numero di byte letti
int FAT32_read(FileHandle* f, void* data, int size);

//sposta il puntatore al file in posizione pos 
int FAT32_seek(FileHandle* f, int pos);

//letteralmente il comando cd, con .. ovviamente sale di un livello 
int FAT32_changeDir(DirectoryHandle* d, char* dirname);

//crea una nuova cartella in quella in cui si sta 
int FAT32_mkDir(DirectoryHandle* d, char* dirname);

//rimuove il file dalla cartella, se è una cartella rimuove anche tutti i file al suo interno  
int FAT32_remove(DirectoryHandle* d, char* filename);	
	
