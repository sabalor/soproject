#include "disk_driver.h"
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <assert.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>

void Disk_Init(DiskDriver* disk, char* filename, int n_block) {
	if (disk == NULL || filename == NULL || n_block < 1) {
		printf("Errore: parametri sbagliati");
		return; }
	disk->fd = open(filename, O_RDWR | O_CREAT | O_TRUNC, 0666);
	if (disk->fd < 0) {
		printf("Errore: fd open");
		return; }
	off_t page = (off_t)sysconf(_SC_PAGESIZE);
	int trunc = ftruncate(disk->fd, page + n_block*sizeof(int) + n_block*BLOCK_SIZE);
	if (trunc < 0) {
		printf("Errore: ftruncate");
		return; }
	disk->header = mmap(NULL, sizeof(DiskDriver_Header), PROT_READ | PROT_WRITE, MAP_SHARED, disk->fd, 0);
	if (disk->header == MAP_FAILED) {
		printf("Errore: MAP_FAILED");
		return; }
	disk->header->n_blocchi = n_block;
	disk->header->blocchi_liberi = n_block;
	disk->header->primo_blocco_libero = 0;
	disk->fat = mmap(NULL, n_block*sizeof(int), PROT_READ | PROT_WRITE, MAP_SHARED, disk->fd, page);
	if (disk->fat == MAP_FAILED) {
		printf("Errore: MAP_FAILED");
		return; }
	for (int idx = 0; idx < n_block; idx++) 
		disk->fat[idx] = -1;
}

//legge il blocco in posizone num_block, ritorna -1 in caso di errore	
int Disk_Read(DiskDriver* disk, void* src, int num_block) {
	if (disk == NULL || src == NULL || num_block < 0) {
		printf("Errore: parametri sbagliati");
		return -1; }
	if (num_block > disk->header->n_blocchi) {
		printf("Errore: limite blocchi");
		return -1; }
	off_t page = (off_t)sysconf(_SC_PAGESIZE);
	off_t offset = lseek(disk->fd, page + num_block*sizeof(int) + num_block*BLOCK_SIZE, SEEK_SET);
	if (offset == -1) {
		printf("Errore: lseek");
		return -1; }
	int ret, bytes_read;
	while (bytes_read < BLOCK_SIZE) {
		ret = read(disk->fd, src + bytes_read, BLOCK_SIZE - bytes_read);
		if (ret == -1 && errno == EINTR) continue;
		if (ret == -1) {
			printf("Errore impossibile leggere il blocco");
			return -1; }
		bytes_read += ret;
	}
	return 0;
}

int Disk_Write(DiskDriver* disk, void* src, int num_block) {
	if (disk == NULL || src == NULL || num_block < 0) {
		printf("Errore: parametri sbagliati");
		return -1; }
	if (num_block > disk->header->n_blocchi) {
		printf("Errore: limite blocchi");
		return -1; }
	if (num_block == disk->header->primo_blocco_libero)
		disk->header->primo_blocco_libero = Disk_getFree(disk, disk->header->primo_blocco_libero +1);
	off_t page = (off_t)sysconf(_SC_PAGESIZE);
	off_t offset = lseek(disk->fd, page + num_block*sizeof(int) + num_block*BLOCK_SIZE, SEEK_SET);
	if (offset == -1) {
		printf("Errore: lseek");
		return -1; }
	int ret, written_bytes;
	while (written_bytes < BLOCK_SIZE) {
		ret = write(disk->fd, src + written_bytes, BLOCK_SIZE - written_bytes);
		if (ret == -1 && errno == EINTR) continue;
		if (ret == -1) {
			printf("Errore: impossibile scrivere sul blocco");
			return -1; }
		written_bytes += ret;
	}
	return 0;
}

int Disk_Flush(DiskDriver* disk) {
	int fat_size = disk->header->n_blocchi/8 +1;
	int ret = msync(disk->header, (size_t)sizeof(DiskDriver_Header), MS_SYNC);
	if (ret == -1) {
		printf("Errore: msync"); 
		return -1; }
	return 0;
}

int Disk_Free(DiskDriver* disk, int num_block) {
	if (disk->header->n_blocchi < num_block || num_block < 0 || disk == NULL) {
		printf("Errore: parametri errati");
		return -1; }
	off_t page = (off_t)sysconf(_SC_PAGESIZE);
	off_t offset = lseek(disk->fd, page + num_block*sizeof(int) + num_block*BLOCK_SIZE, SEEK_SET);
	if (offset == -1) {
		printf("Errore: lseek");
		return -1; }
	int* free_block = (int*)calloc(BLOCK_SIZE, sizeof(int));
	int ret, written_bytes;
	while (written_bytes < BLOCK_SIZE) {
		ret = write(disk->fd, free_block + written_bytes, BLOCK_SIZE - written_bytes);
		if (ret == -1 && errno == EINTR) continue;
		if (ret == -1) {
			printf("Errore: impossibile scrivere sul blocco");
			return -1; }
		}
	if (disk->header->primo_blocco_libero > num_block)
		disk->header->primo_blocco_libero = num_block;
	return 0;
}
//restituisce il primo blocco libero a partire dalla posizione pos
int Disk_getFree(DiskDriver* disk, int pos) {
	if (pos > disk->header->n_blocchi || pos < 0) {
		printf("Errore: posizione non valida"); 
		return -1; }
	if (disk->header->blocchi_liberi == 0) {
		printf("Errore: nessun blocco libero");
		return -1; }
	for (int i = pos; i < disk->header->n_blocchi; i++) {
		if (disk->fat[i] == -1) {
			disk->header->blocchi_liberi -= 1;
			return i; }
	}
	return -1;
}
	
	
	
	
