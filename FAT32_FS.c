#include "FAT32_FS.h"
#include <stdlib.h>
#include <stdio.h>

DirectoryHandle* FAT32_init(FAT32* fat, DiskDriver* disk) {
	if (disk == NULL || fat == NULL) {
		printf("Errore: parametri sbagliati");
		return NULL; }
	fat->disk = disk;
	FirstDirectoryBlock* fdb = (FirstDirectoryBlock*)malloc(sizeof(FirstDirectoryBlock));
	//leggo il blocco 0 del disco(root)
	int ret = Disk_Read(disk, (void*)fdb, 0);
	//no root directory
	if (ret == -1) {
		free(fdb);
		return NULL; }
	fat->fdp = fdb;	
	DirectoryHandle* dh = (DirectoryHandle*)malloc(sizeof(DirectoryHandle));
	dh->fat = fat;
	dh->dcb = fdb;
	dh->directory = NULL;
	dh->pos_in_block = 0;
	return dh;
}
	
	
