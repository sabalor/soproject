#pragma once
#define BLOCK_SIZE 512
#define DISK_SIZE 1048576

typedef struct {
	int n_blocchi;
	int blocchi_liberi;
	int primo_blocco_libero;
} DiskDriver_Header;

typedef struct {
	DiskDriver_Header* header;
	int* fat;
	int fd;
} DiskDriver;

//FUNZIONI DA IMPLEMENTARE

//inizializza il disco
void Disk_Init(DiskDriver* disk, char* filename, int n_block);

//legge il blocco in posizione n_block, ritorna -1 se non possibile
int Disk_Read(DiskDriver* disk, void* src, int num_block);

//scrive un blocco in posizione n_block, -1 se non possibile
int Disk_Write(DiskDriver* disk, void* src, int num_block);

//libera il blocco in posizione n_block
int Disk_Free(DiskDriver* disk, int num_block);

//ritorna il primo blocco libero a partire da pos
int Disk_getFree(DiskDriver* disk, int pos);

//flushing
int Disk_Flush(DiskDriver* disk);
