#include "linked_list.h"
#include <stdlib.h>
#include <stdio.h>

Node* createNode(int data) {
	Node* new = (Node*)malloc(sizeof(Node));
	new->data = data;
	new->next = NULL;
	return new;
}

List* createList() {
	List* l = (List*)malloc(sizeof(List));
	l->head = NULL;
	return l;
}

void addNode(int data, List* l) {
	Node* current = NULL;
	if (l->head == NULL) {
		l->head = createNode(data); }
	else {
		current = l->head;
		while (current->next != NULL) {
			current = current->next; }
		current = createNode(data);
	}
	return;
}

void deleteNode(int data, List* l) {
	Node* current = l->head;
	Node* previous = current;
	while (current != NULL) {
		if (current->data == data) {
			previous->next = current->next;
			if (current == l->head)
				l->head = current->next;
			free(current);
			return; }
		previous = current;
		current = current->next;
	}
}

void destroyList(List* l) {
	Node* current = l->head;
	Node* next = current;
	while (current != NULL) {
		next = current->next;
		free(current);
		current = next;
	}
	free(l);
}
			
			
