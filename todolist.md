# TODO

## createFile
  * [ ] Accesso a DISK
  * [ ] Controllo se esiste già un file con quel nome nella directory in cui si trova
    * [ ] Controllo che sia un file
  * [ ] Controllo se c'è spazio nella FAT
  * [ ] Controllo se ho abbastanza memoria per allocare
  * [ ] Creo il file
  * [ ] Alloco la memoria
  * [ ] Aggiungo il puntatore alla FAT
  * [ ] Restituisco il FileHandle
## eraseFile
  * [ ] Accesso a DISK
  * [ ] Controllo se esiste già un file con quel nome nella directory in cui si trova
    * [ ] Controllo che sia un file
    * [ ] Cerco il puntatore al file nella FAT
  * [ ] De alloco la memoria
  * [ ] Rimuovo riferimenti nella FAT
## write (potentially extending the file boundaries)
  * [ ] Accesso a DISK
  * [ ] Controllo se esiste già un file con quel nome nella directory in cui si trova
    * [ ] Controllo che sia un file
    * [ ] Cerco il puntatore al file nella FAT
  * [ ] Controllo se ho abbastanza memoria per allocare
    * [ ] Controllo se mi servono più blocchi per salvare il file
  * [ ] Alloco la memoria
## read
  * [ ] Accesso a DISK
  * [ ] Controllo se esiste già un file con quel nome nella directory in cui si trova
    * [ ] Controllo che sia un file
    * [ ] Cerco il puntatore al file nella FAT
  * [ ] Prendo prendo il contenuto e lo salvo su buffer
    * [ ] Crea buffer
    * [ ] Scrivi buffer
    * [ ] Ripulisci buffer
    * [ ] Elimina buffer
  * [ ] Stampo a video 
## seek
## createDir
  * [ ] Accesso a DISK
  * [ ] Controllo se esiste già un file con quel nome nella directory in cui si trova
    * [ ] Controllo che sia una directory
  * [ ] createFile
  * [ ] Segno il file come directory nella FAT delle directory
## eraseDir
  * [ ] Accesso a DISK
  * [ ] Controllo se esiste già un file con quel nome nella directory in cui si trova
    * [ ] Controllo che sia una directory
  * [ ] eraseFile
  * [ ] Elimino il file come directory nella FAT delle directory
## changeDir
  * [ ] Accesso a DISK
  * [ ] In base alla directory selezionata scorro i padri e/o i figli
  * [ ] Controllo se esiste già un file con quel nome nella directory in cui si trova
    * [ ] Controllo che sia una directory
  * [ ] Cambio il puntatore della directory corrente con quella selezionata
## listDir
  * [ ] Accesso a DISK
  * [ ] print della lista dei file e directory figlie della directory corrente

_The opening of a file should return a "FileHandle" that stores the position in a file._

### Accesso a DISK
  * [ ] Controllare che il disco esista
  * [ ] Mettere il puntatore al primo blocco del disco
  * [ ] Restituisce il puntatore
### Controllo se esiste già un file con quel nome nella directory in cui si trova
  * [ ] Accedo alla FAT
  * [ ] Salvo una lista dei file nella directory selezionata
  * [ ] Per ogni file nella lista controllo il nome
    * [ ] In base alla richiesta controllo se è anche un file o una directory
  * [ ] Restituisco il risultato della ricerca


